class Student
  attr_accessor :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @last_name = last_name
    @first_name = first_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def has_conflict?(new_course)
    @courses.each do |x|
      return true if x.conflicts_with?(new_course)
    end
    false
  end

  def enroll(new_course)
    raise Exception.new("new course conflicts with existing courses") if has_conflict?(new_course)
    unless @courses.include?(new_course)
      @courses << new_course
      unless new_course.students.include?(self)
        new_course.students << self
      end
    end
  end

  def courses
    @courses
  end

  def course_load
    breakdown = Hash.new(0)
    @courses.each do |x|
      breakdown[x.department] += x.credits
    end
    breakdown
  end

end
